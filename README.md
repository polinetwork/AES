# Aerospaziale
Ciao!
Questa è la repository dedicata alla Laurea Triennale e Magistrale di Ingegneria Aerospaziale (Polimi) gestita dal [PoliNetwork](https://polinetwork.github.io/it/index.html)

Vi troverete materiale riguardante i vari corsi e volendo potete caricarlo anche voi attraverso il bot telegram [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

**Triennale**
 * Primo anno https://gitlab.com/polinetwork/AES1y
 * Secondo anno https://gitlab.com/polinetwork/AES2y
 * Terzo anno https://gitlab.com/polinetwork/AES3y
 * Corsi a scelta https://gitlab.com/polinetwork/AES_corsi6cfu


**Magistrale Aeronautica**
-> Per creare le repository della magistrale in aeronautica scrivete al [responsabile admin di aerospaziale](https://polinetwork.github.io/it/about_us/index.html)


**Magistrale Spazio**
 * Primo anno https://gitlab.com/polinetwork/space1y
 * Secondo anno https://gitlab.com/polinetwork/space2y


Per qualunque info scrivete al [responsabile admin di aerospaziale](https://polinetwork.github.io/it/about_us/index.html)


 -----------------

Hello everyone this is the repository of BSc and MSc in Aerospace Engineering (Polimi) reserved by [PoliNetwork](https://polinetwork.github.io/it/index.html)

You will find and can upload the material for the courses (Notes, Exercises, ...).
 To upload files use the Telegram Bot [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

**Bachelor**
 * First year https://gitlab.com/polinetwork/AES1y
 * Second year https://gitlab.com/polinetwork/AES2y
 * Third year https://gitlab.com/polinetwork/AES3y
 * Elective courses https://gitlab.com/polinetwork/AES_corsi6cfu


**MSc Aeronautical**
-> To create a repository for aeronautical Engineering contact [aerospace head admin](https://polinetwork.github.io/en/about_us/index.html)


**MSc Space**
 * First year https://gitlab.com/polinetwork/space1y
 * Secondo year https://gitlab.com/polinetwork/space2y


For any query contact [aerospace head admin](https://polinetwork.github.io/en/about_us/index.html)

**ATTENTION**: You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.
